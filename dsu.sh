# Instructions to build a Data Science VM based on Ubuntu Xenial

######################################
# Pre-requisites
######################################

# Install Virtual Box 5.1+
# https://www.virtualbox.org/

# Install Ubuntu 16.04.1 LTS on VirtualBox
# http://www.ubuntu.com/download/desktop

# Setup a shared folder between host (Windows) and Guest (Ubuntu VM)
# https://www.youtube.com/watch?v=TcrfrVNNGMU


######################################
# R
######################################

# Update CRAN mirror for R
echo "deb https://cran.rstudio.com/bin/linux/ubuntu xenial/" | sudo tee -a /etc/apt/sources.list
## Secure CRAN mirror and upgrade
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9
sudo apt-get update
sudo apt-get upgrade

# install R and R compiler
sudo apt-get install r-base r-base-dev

# install R packages from CRAN
sudo su - -c "R -e \"install.packages('DescTools', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('dplyr', repos='http://cran.rstudio.com/')\""

# Install R packages from github
## Install dependencies for devtools
sudo apt-get -y install libcurl4-gnutls-dev
sudo apt-get -y install libxml2-dev
sudo apt-get -y install libssl-dev
## install devtools
sudo su - -c "R -e \"install.packages('devtools', repos='http://cran.rstudio.com/')\""
## example
#sudo su - -c "R -e \"devtools::install_github('daattali/shinyjs')\""


######################################
# RStudio
######################################

# install RStudio
# NOTE: change to latest version at https://www.rstudio.com/products/rstudio/download2/
sudo apt-get install gdebi-core
wget https://download1.rstudio.org/rstudio-0.99.903-amd64.deb
sudo gdebi -n rstudio-0.99.903-amd64.deb

# install RStudio Server
wget https://download2.rstudio.org/rstudio-server-0.99.903-amd64.deb
sudo gdebi rstudio-server-0.99.903-amd64.deb


######################################
# Shiny Server
######################################

# Install Shiny Server to run Shiny apps and RMarkdown files
## first install the 'shiny' and 'rmarkdown' packages
sudo su - -c "R -e \"install.packages('shiny', repos='http://cran.rstudio.com/')\""
sudo su - -c "R -e \"install.packages('rmarkdown', repos='http://cran.rstudio.com/')\""
## then download and install the app from https://www.rstudio.com/products/shiny/download-server/
wget https://download3.rstudio.org/ubuntu-12.04/x86_64/shiny-server-1.4.4.801-amd64.deb
sudo gdebi shiny-server-1.4.4.801-amd64.deb
## Check if server is running at http://localhost:3838/
## Admin tutorial: http://docs.rstudio.com/shiny-server/


######################################
# Anaconda
######################################

# Install Anaconda
# NOTE: change to latest version https://www.continuum.io/downloads#_unix
wget http://repo.continuum.io/archive/Anaconda3-4.1.1-Linux-x86_64.sh
bash Anaconda3-4.1.1-Linux-x86_64.sh
## Type spyder to run GUI

# install R-Essentials through conda (R-kernel for jupyter and R packages)
# https://docs.continuum.io/anaconda/r_language
conda install -c r r-essentials
## to update packages: http://conda.pydata.org/docs/r-with-conda.html
## TODO: alternative method to install kernel with less dependencies
## Type ipython notebook 


######################################
# Databases
######################################

## SQLite
sudo apt-get install -y sqlite3 libsqlite3-dev


######################################
# Version control
######################################

# Install git
sudo apt-get -y install git-core
## config git: http://r-pkgs.had.co.nz/git.html


######################################
# Docker
######################################

# Install Docker
## Add official GPG key
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
## Add the Docker repository
echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt-get update
## Make sure you are about to install from the Docker repo instead of the default Ubuntu 16.04 repo:
apt-cache policy docker-engine
## Install the linux-image-extra kernel package to use the aufs storage driver.
sudo apt-get install linux-image-extra-$(uname -r)
## Install Docker and check running status
sudo apt-get install -y docker-engine
sudo systemctl status docker
## Add your username to the docker group to avoid typing sudo whenever you run the docker command
sudo usermod -aG docker $(whoami)
## Start the Docker deamon and verify if it is installed correctly
sudo service docker start
docker run hello-world
